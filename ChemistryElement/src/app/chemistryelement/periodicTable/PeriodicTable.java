/* Chemistry Elements version 2
 * al.noman.uap@gmail.com*/

package app.chemistryelement.periodicTable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import app.chemistryElements.periodicTable.detail.ElementDetail;
import app.chemistryElements.periodicTable.search.Search;

public class PeriodicTable extends Activity {

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_periodic_table);

	}

	public void click(View view) {

		switch (view.getId()) {

		case R.id.H:

			this.showElementDetails("h.xml", R.drawable.orb_h);
			break;

		case R.id.He:

			this.showElementDetails("he.xml", R.drawable.orb_he);
			break;

		case R.id.Li:

			this.showElementDetails("li.xml", R.drawable.orb_li);
			break;

		case R.id.Be:

			this.showElementDetails("be.xml", R.drawable.orb_be);
			break;

		case R.id.B:

			this.showElementDetails("b.xml", R.drawable.orb_b);
			break;

		case R.id.C:

			this.showElementDetails("c.xml", R.drawable.orb_c);
			break;

		case R.id.N:

			this.showElementDetails("n.xml", R.drawable.orb_n);
			break;

		case R.id.O:

			this.showElementDetails("o.xml", R.drawable.orb_o);
			break;

		case R.id.F:

			this.showElementDetails("f.xml", R.drawable.orb_f);
			break;

		case R.id.Ne:

			this.showElementDetails("ne.xml", R.drawable.orb_ne);
			break;

		case R.id.Na:

			this.showElementDetails("na.xml", R.drawable.orb_na);
			break;

		case R.id.Mg:

			this.showElementDetails("mg.xml", R.drawable.orb_mg);
			break;

		case R.id.Al:

			this.showElementDetails("al.xml", R.drawable.orb_al);
			break;

		case R.id.Si:

			this.showElementDetails("si.xml", R.drawable.orb_si);
			break;

		case R.id.P:

			this.showElementDetails("p.xml", R.drawable.orb_p);
			break;

		case R.id.S:

			this.showElementDetails("s.xml", R.drawable.orb_s);
			break;

		case R.id.Cl:

			this.showElementDetails("cl.xml", R.drawable.orb_cl);
			break;

		case R.id.Ar:

			this.showElementDetails("ar.xml", R.drawable.orb_ar);
			break;

		case R.id.K:

			this.showElementDetails("k.xml", R.drawable.orb_k);
			break;

		case R.id.Ca:

			this.showElementDetails("ca.xml", R.drawable.orb_ca);
			break;

		case R.id.Sc:

			this.showElementDetails("sc.xml", R.drawable.orb_sc);
			break;

		case R.id.Ti:

			this.showElementDetails("ti.xml", R.drawable.orb_ti);
			break;

		case R.id.V:

			this.showElementDetails("v.xml", R.drawable.orb_v);
			break;

		case R.id.Cr:

			this.showElementDetails("cr.xml", R.drawable.orb_cr);
			break;

		case R.id.Mn:

			this.showElementDetails("mn.xml", R.drawable.orb_mn);
			break;

		case R.id.Fe:

			this.showElementDetails("fe.xml", R.drawable.orb_fe);
			break;

		case R.id.Co:

			this.showElementDetails("co.xml", R.drawable.orb_co);
			break;

		case R.id.Ni:

			this.showElementDetails("ni.xml", R.drawable.orb_ni);
			break;

		case R.id.Cu:

			this.showElementDetails("cu.xml", R.drawable.orb_cu);
			break;

		case R.id.Zn:

			this.showElementDetails("zn.xml", R.drawable.orb_zn);
			break;

		case R.id.Ga:

			this.showElementDetails("ga.xml", R.drawable.orb_ga);
			break;

		case R.id.Ge:

			this.showElementDetails("ge.xml", R.drawable.orb_ge);
			break;

		case R.id.As:

			this.showElementDetails("as.xml", R.drawable.orb_as);
			break;

		case R.id.Se:

			this.showElementDetails("se.xml", R.drawable.orb_se);
			break;

		case R.id.Br:

			this.showElementDetails("br.xml", R.drawable.orb_br);
			break;

		case R.id.Kr:

			this.showElementDetails("kr.xml", R.drawable.orb_kr);
			break;

		case R.id.Rb:

			this.showElementDetails("rb.xml", R.drawable.orb_rb);
			break;

		case R.id.Sr:

			this.showElementDetails("sr.xml", R.drawable.orb_sr);
			break;

		case R.id.Y:

			this.showElementDetails("y.xml", R.drawable.orb_y);
			break;

		case R.id.Zr:

			this.showElementDetails("zr.xml", R.drawable.orb_zr);
			break;

		case R.id.Nb:

			this.showElementDetails("nb.xml", R.drawable.orb_nb);
			break;

		case R.id.Mo:

			this.showElementDetails("mo.xml", R.drawable.orb_mo);
			break;

		case R.id.Tc:

			this.showElementDetails("tc.xml", R.drawable.orb_tc);
			break;

		case R.id.Ru:

			this.showElementDetails("ru.xml", R.drawable.orb_ru);
			break;

		case R.id.Rh:

			this.showElementDetails("rh.xml", R.drawable.orb_rh);
			break;

		case R.id.Pd:

			this.showElementDetails("pd.xml", R.drawable.orb_pd);
			break;

		case R.id.Ag:

			this.showElementDetails("ag.xml", R.drawable.orb_ag);
			break;

		case R.id.Cd:

			this.showElementDetails("cd.xml", R.drawable.orb_cd);
			break;

		case R.id.In:

			this.showElementDetails("in.xml", R.drawable.orb_in);
			break;

		case R.id.Sn:

			this.showElementDetails("sn.xml", R.drawable.orb_sn);
			break;

		case R.id.Sb:

			this.showElementDetails("sb.xml", R.drawable.orb_sb);
			break;

		case R.id.Te:

			this.showElementDetails("te.xml", R.drawable.orb_te);
			break;

		case R.id.I:

			this.showElementDetails("i.xml", R.drawable.orb_i);
			break;

		case R.id.Xe:

			this.showElementDetails("xe.xml", R.drawable.orb_xe);
			break;

		case R.id.Cs:

			this.showElementDetails("cs.xml", R.drawable.orb_cs);
			break;

		case R.id.Ba:

			this.showElementDetails("ba.xml", R.drawable.orb_ba);
			break;

		case R.id.la:

			this.showElementDetails("la.xml", R.drawable.orb_la);
			break;

		case R.id.Hf:

			this.showElementDetails("hf.xml", R.drawable.orb_hf);
			break;

		case R.id.Ta:

			this.showElementDetails("ta.xml", R.drawable.orb_ta);
			break;

		case R.id.W:

			this.showElementDetails("w.xml", R.drawable.orb_w);
			break;

		case R.id.Re:

			this.showElementDetails("re.xml", R.drawable.orb_re);
			break;

		case R.id.Os:

			this.showElementDetails("os.xml", R.drawable.orb_os);
			break;

		case R.id.Ir:

			this.showElementDetails("ir.xml", R.drawable.orb_ir);
			break;

		case R.id.Pt:

			this.showElementDetails("pt.xml", R.drawable.orb_pt);
			break;

		case R.id.Au:

			this.showElementDetails("au.xml", R.drawable.orb_au);
			break;

		case R.id.Hg:

			this.showElementDetails("hg.xml", R.drawable.orb_hg);
			break;

		case R.id.Tl:

			this.showElementDetails("tl.xml", R.drawable.orb_tl);
			break;

		case R.id.Pb:

			this.showElementDetails("pb.xml", R.drawable.orb_pb);
			break;

		case R.id.Bi:

			this.showElementDetails("bi.xml", R.drawable.orb_bi);
			break;

		case R.id.Po:

			this.showElementDetails("po.xml", R.drawable.orb_po);
			break;

		case R.id.At:

			this.showElementDetails("at.xml", R.drawable.orb_at);
			break;

		case R.id.Rn:

			this.showElementDetails("rn.xml", R.drawable.orb_rn);
			break;

		case R.id.Fr:

			this.showElementDetails("fr.xml", R.drawable.orb_fr);
			break;

		case R.id.Ra:

			this.showElementDetails("ra.xml", R.drawable.orb_ra);
			break;

		case R.id.Ac:

			this.showElementDetails("ac.xml", R.drawable.orb_ac);
			break;

		case R.id.Rf:

			this.showElementDetails("rf.xml", R.drawable.orb_rf);
			break;

		case R.id.Db:

			this.showElementDetails("db.xml", R.drawable.orb_db);
			break;

		case R.id.Sg:

			this.showElementDetails("sg.xml", R.drawable.orb_sg);
			break;

		case R.id.Bh:

			this.showElementDetails("bh.xml", R.drawable.orb_bh);
			break;

		case R.id.Hs:

			this.showElementDetails("hs.xml", R.drawable.orb_hs);
			break;

		case R.id.Mt:

			this.showElementDetails("mt.xml", R.drawable.orb_mt);
			break;

		case R.id.Ds:

			this.showElementDetails("ds.xml", R.drawable.orb_ds);
			break;

		case R.id.Rg:

			this.showElementDetails("rg.xml", R.drawable.orb_rg);
			break;

		case R.id.Cn:

			this.showElementDetails("cn.xml", R.drawable.orb_cn);
			break;

		case R.id.Uut:

			this.showElementDetails("uut.xml", R.drawable.orb_uut);
			break;

		case R.id.Fl:

			this.showElementDetails("fl.xml", R.drawable.orb_fl);
			break;

		case R.id.Ce:

			this.showElementDetails("ce.xml", R.drawable.orb_ce);
			break;

		case R.id.Pr:

			this.showElementDetails("pr.xml", R.drawable.orb_pr);
			break;

		case R.id.Nd:

			this.showElementDetails("nd.xml", R.drawable.orb_nd);
			break;

		case R.id.Pm:

			this.showElementDetails("pm.xml", R.drawable.orb_pm);
			break;

		case R.id.Sm:

			this.showElementDetails("sm.xml", R.drawable.orb_sm);
			break;

		case R.id.Eu:

			this.showElementDetails("eu.xml", R.drawable.orb_eu);
			break;

		case R.id.Gd:

			this.showElementDetails("gd.xml", R.drawable.orb_gd);
			break;

		case R.id.Tb:

			this.showElementDetails("tb.xml", R.drawable.orb_tb);
			break;

		case R.id.Dy:

			this.showElementDetails("dy.xml", R.drawable.orb_dy);
			break;

		case R.id.Ho:

			this.showElementDetails("ho.xml", R.drawable.orb_ho);
			break;

		case R.id.Er:

			this.showElementDetails("er.xml", R.drawable.orb_er);
			break;

		case R.id.Tm:

			this.showElementDetails("tm.xml", R.drawable.orb_tm);
			break;

		case R.id.Yb:

			this.showElementDetails("yb.xml", R.drawable.orb_yb);
			break;

		case R.id.Lu:

			this.showElementDetails("lu.xml", R.drawable.orb_lu);
			break;

		case R.id.Th:

			this.showElementDetails("th.xml", R.drawable.orb_th);
			break;

		case R.id.Pa:

			this.showElementDetails("pa.xml", R.drawable.orb_pa);
			break;

		case R.id.U:

			this.showElementDetails("u.xml", R.drawable.orb_u);
			break;

		case R.id.Np:

			this.showElementDetails("np.xml", R.drawable.orb_np);
			break;

		case R.id.Pu:

			this.showElementDetails("pu.xml", R.drawable.orb_pu);
			break;

		case R.id.Am:

			this.showElementDetails("am.xml", R.drawable.orb_am);
			break;

		case R.id.Cm:

			this.showElementDetails("cm.xml", R.drawable.orb_cm);
			break;

		case R.id.Bk:

			this.showElementDetails("bk.xml", R.drawable.orb_bk);
			break;

		case R.id.Cf:

			this.showElementDetails("cf.xml", R.drawable.orb_cf);
			break;

		case R.id.Es:

			this.showElementDetails("es.xml", R.drawable.orb_es);
			break;

		case R.id.Fm:

			this.showElementDetails("fm.xml", R.drawable.orb_fm);
			break;

		case R.id.Md:

			this.showElementDetails("md.xml", R.drawable.orb_md);
			break;

		case R.id.No:

			this.showElementDetails("no.xml", R.drawable.orb_no);
			break;

		case R.id.Lr:

			this.showElementDetails("lr.xml", R.drawable.orb_lr);
			break;

		case R.id.Uup:

			this.showElementDetails("uup.xml", R.drawable.orb_uup);
			break;

		case R.id.Lv:

			this.showElementDetails("lv.xml", R.drawable.orb_lv);
			break;

		case R.id.Uus:

			this.showElementDetails("uus.xml", R.drawable.orb_uus);
			break;

		case R.id.Uuo:

			this.showElementDetails("uuo.xml", R.drawable.orb_uuo);
			break;
		}
	}

	public void showElementDetails(String elementName, int id) {

		Intent intent = new Intent(PeriodicTable.this, ElementDetail.class);

		intent.putExtra("element", elementName);
		intent.putExtra("picsId", id);

		startActivity(intent);
		finish();

	}

	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.periodic_table_menu, menu);

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.search:

			startActivity(new Intent(PeriodicTable.this, Search.class));
			finish();
			break;

		}

		return super.onOptionsItemSelected(item);
	}

}
