package app.chemistryElements.periodicTable.search;

import java.io.InputStreamReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import app.chemistryelement.periodicTable.PeriodicTable;
import app.chemistryelement.periodicTable.R;

public class Search extends Activity {

	private Button search;
	private EditText editText;
	private TextView displayText;
	private ImageView imageView;

	private XmlPullParserFactory xppFactory;
	private XmlPullParser xpParser;

	private int eventType;

	private String symbol, text, xml, image;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_element);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		editText = (EditText) findViewById(R.id.searchBox);

		displayText = (TextView) findViewById(R.id.show_detail2);

		imageView = (ImageView) findViewById(R.id.pics_orb2);

		search = (Button) findViewById(R.id.search_btn);
		search.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {

				symbol = editText.getText().toString();
				symbol = symbol.toLowerCase();
				xml = symbol.concat(".xml");
				image = "orb_".concat(symbol);

				this.setImage(image);

				try {

					xppFactory = XmlPullParserFactory.newInstance();
					xpParser = xppFactory.newPullParser();

					eventType = xpParser.getEventType();

					xpParser.setInput(new InputStreamReader(
							getApplicationContext().getAssets().open(xml),
							"UTF-8"));

					text = "";

					while (eventType != XmlPullParser.END_DOCUMENT) {

						if (eventType == XmlPullParser.TEXT) {
							text = text + "" + xpParser.getText();
						}

						eventType = xpParser.next();
					}
					displayText.setText(text);

				} catch (Exception error) {
					Toast.makeText(Search.this, "Not Found", Toast.LENGTH_SHORT)
							.show();
				}

			}

			private void setImage(String img) {

				try {
					String pics = "drawable/" + img;

					int imageRes = getResources().getIdentifier(pics, null,
							getPackageName());
					imageView = (ImageView) findViewById(R.id.pics_orb2);

					Drawable image = getResources().getDrawable(imageRes);
					imageView.setImageDrawable(image);
				} catch (Exception error) {
					Toast.makeText(Search.this, "Not Found", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});

	}


	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:

			startActivity(new Intent(Search.this, PeriodicTable.class));
			finish();
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(Search.this, PeriodicTable.class));
		finish();
		super.onBackPressed();
	}

}
