package app.chemistryElements.periodicTable.detail;

import java.io.InputStreamReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import app.chemistryElements.periodicTable.search.Search;
import app.chemistryelement.periodicTable.PeriodicTable;
import app.chemistryelement.periodicTable.R;

public class ElementDetail extends Activity {

	private XmlPullParserFactory xppFactory;
	private XmlPullParser xpParser;

	private int eventType, id;

	private TextView displayText;
	private ImageView imageView;

	private Intent intent;
	private String xml, text;

	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.info_detail);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		try {
			xppFactory = XmlPullParserFactory.newInstance();

			xpParser = xppFactory.newPullParser();

			eventType = xpParser.getEventType();

			displayText = (TextView) findViewById(R.id.show_detail);
			imageView = (ImageView) findViewById(R.id.pics_orb);

			intent = getIntent();

			xml = intent.getExtras().getString("element");
			id = intent.getExtras().getInt("picsId");

			imageView.setBackgroundResource(id); // set image

			xpParser.setInput(new InputStreamReader(getApplicationContext()
					.getAssets().open(xml), "UTF-8"));

			text = "";
			while (eventType != XmlPullParser.END_DOCUMENT) {

				if (eventType == XmlPullParser.TEXT) {
					text = text + "" + xpParser.getText();
				}
				eventType = xpParser.next();
			}

			displayText.setText(text); // set parsed xml

		} catch (Exception e) {
			Toast.makeText(ElementDetail.this, "Details Not Found",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.periodic_table_menu, menu);

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.search:

			startActivity(new Intent(ElementDetail.this, Search.class));
			finish();
			break;

		case android.R.id.home:

			startActivity(new Intent(ElementDetail.this, PeriodicTable.class));
			finish();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(ElementDetail.this, PeriodicTable.class));
		finish();
		super.onBackPressed();
	}

}
